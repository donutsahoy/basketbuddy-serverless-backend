import { QueryObjectResult } from "https://deno.land/x/postgres@v0.14.2/query/query.ts";
import { ResponseBuilder } from "../__shared/responses.ts";
import { generateShareCode } from "../__shared/shareCodeGenerator.ts";
import { OptionalString } from "../__shared/types.ts";
import * as postgres from "https://deno.land/x/postgres@v0.14.2/mod.ts";

const databaseUrl = Deno.env.get("SUPABASE_DB_URL")!;
const pool = new postgres.Pool(databaseUrl, 3, true);

export async function createShare(itemsList: string) {
  if (itemsList === undefined || itemsList === null) {
    return ResponseBuilder.responseMissingRequiredFields();
  }

  const connection = await pool.connect();

  try {
    const expirationMS = new Date().getTime() + 15 * 60 * 1000;

    const expirationDateString = new Date(expirationMS).toISOString();

    let isCodeInUse = true;
    let shareCode = "";
    let killswitch = 0;

    do {
      shareCode = generateShareCode();

      isCodeInUse = ((await connection.queryObject(
        `SELECT EXISTS(SELECT 1 FROM public.shared_items WHERE share_code = $1);`,
        shareCode,
      )) as QueryObjectResult<{ exists: boolean }>)?.rows?.[0]?.exists;

      killswitch++;
      if (killswitch > 100) {
        return ResponseBuilder.response500("Could not generate share code.");
      }
    } while (isCodeInUse);

    const result = (await connection.queryObject(
      `INSERT INTO public.shared_items (expiration, "data", share_code) VALUES($1, $2, $3) RETURNING share_code`,
      expirationDateString,
      JSON.stringify(itemsList),
      shareCode,
    )) as QueryObjectResult<{ share_code: string }>;

    return ResponseBuilder.response201({
      shareCode: result?.rows[0]?.share_code,
    });
  } catch (error) {
    console.error(error);
    return ResponseBuilder.response500(error.message);
  } finally {
    connection.release();
  }
}

export async function acceptShare(shareCode: OptionalString) {
  const connection = await pool.connect();
  try {
    const result = (await connection.queryObject(
      `SELECT * FROM public.shared_items WHERE share_code = $1`,
      shareCode,
    )) as QueryObjectResult<{ data: string; expiration: string }>;

    if (result?.rows[0]?.data) {
      await connection.queryObject(
        `DELETE FROM public.shared_items WHERE share_code = $1`,
        shareCode,
      );

      if (new Date(result?.rows[0]?.expiration) < new Date()) {
        return ResponseBuilder.response400("Share code has expired.");
      }

      return ResponseBuilder.response201({ data: result?.rows[0]?.data });
    }

    return ResponseBuilder.response404();
  } catch (error) {
    console.error(error);
    return ResponseBuilder.response500(error.message);
  } finally {
    connection.release();
  }
}

export async function clearExpiredShares() {
  const connection = await pool.connect();

  try {
    (await connection.queryObject(
      `DELETE FROM public.shared_items WHERE expiration < $1`,
      new Date().toISOString(),
    )) as QueryObjectResult<{ data: string; expiration: string }>;

    return ResponseBuilder.response204();
  } catch (error) {
    console.error(error);
    return ResponseBuilder.response500(error.message);
  } finally {
    connection.release();
  }
}
