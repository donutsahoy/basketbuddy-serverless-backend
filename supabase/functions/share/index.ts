import { serve } from "https://deno.land/std@0.168.0/http/server.ts";
import { ResponseBuilder } from "../__shared/responses.ts";
import {
  acceptShare,
  clearExpiredShares,
  createShare,
} from "./share.functions.ts";

serve(async (req: Request) => {
  const { url, method } = req;

  try {
    const createSharePattern = new URLPattern({ pathname: "/share/create" });
    const isCreatePath = createSharePattern.exec(url);

    const acceptSharePattern = new URLPattern({
      pathname: "/share/accept/:shareCode",
    });
    const isAcceptSharePattern = acceptSharePattern.exec(url);

    const clearExpiredPattern = new URLPattern({
      pathname: "/share/clearExpired",
    });
    const isClearExpiredPath = clearExpiredPattern.exec(url);

    if (isCreatePath && method === "POST") {
      const bodyJson = await req.json();
      return createShare(bodyJson?.itemsList);
    } else if (isAcceptSharePattern && method === "POST") {
      const shareCode: string | null | undefined = isAcceptSharePattern
        ? isAcceptSharePattern?.pathname?.groups?.shareCode
        : null;
      return acceptShare(shareCode);
    } else if (isClearExpiredPath && method === "POST") {
      return clearExpiredShares();
    } else {
      return ResponseBuilder.response404();
    }
  } catch (error) {
    return ResponseBuilder.response400(error.message);
  }
});
