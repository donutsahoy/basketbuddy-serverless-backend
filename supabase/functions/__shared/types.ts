export type OptionalString = string | null | undefined;
export type OptionalNumber = number | null | undefined;
