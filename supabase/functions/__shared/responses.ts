import { OptionalString } from "./types.ts";

export class ResponseBuilder {
  static response200(data: Record<string, unknown>) {
    return new Response(JSON.stringify(data), {
      headers: { "Content-Type": "application/json" },
      status: 200,
    });
  }

  static response201(data: Record<string, unknown>) {
    return new Response(JSON.stringify(data), {
      headers: { "Content-Type": "application/json" },
      status: 201,
    });
  }

  static response204() {
    return new Response(null, {
      status: 204,
    });
  }

  static response400(errorMessage: OptionalString) {
    return new Response(JSON.stringify({ error: errorMessage }), {
      headers: { "Content-Type": "application/json" },
      status: 400,
    });
  }

  static responseMissingRequiredFields() {
    return ResponseBuilder.response400("Missing required fields");
  }

  static response401() {
    return new Response(JSON.stringify({ error: "Unauthorized" }), {
      headers: { "Content-Type": "application/json" },
      status: 401,
    });
  }

  static response403() {
    return new Response(
      JSON.stringify({ error: "Not permitted to perform this action." }),
      {
        headers: { "Content-Type": "application/json" },
        status: 403,
      },
    );
  }

  static response404() {
    return new Response(JSON.stringify({ error: "Not Found" }), {
      headers: { "Content-Type": "application/json" },
      status: 404,
    });
  }

  static responseResourceNotFound(errorString: string) {
    return new Response(JSON.stringify({ error: errorString }), {
      headers: { "Content-Type": "application/json" },
      status: 404,
    });
  }

  static response500(errorMessage: OptionalString) {
    return new Response(JSON.stringify({ error: errorMessage }), {
      headers: { "Content-Type": "application/json" },
      status: 500,
    });
  }
}
